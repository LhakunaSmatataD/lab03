/**
* This is the header file and implementation for the names of schools with zero graduates in the tenth grade
*
* @author Abdul Hadi
*
*/

#include <iostream>
#include<fstream>
#include<vector>
#include<string>

using namespace std;

/**
* @class main
* This class implements the names of schools with zero 10th grade graduates
*/

int main()
{
	///initialises string
	string string1;

	ifstream file;
	///opens file by giving address
	file.open("C:/Users/Ahadi/Desktop/Tehsil.csv");

	///initialises vector
	vector <string> vector;

	///creates while loop to push back the file contents into the vector
	while (getline(file, string1, ','))
	{
		vector.push_back(string1);
	}


	/**
	* This function checks whether the file has opened successfully
	*
	* 
	* @return Successful if opens, Error otherwise
	*/
	for (int i = 12; i < vector.size(); i += 13) {
		if (vector[i] == "0.00") {
			cout << vector[i - 9] << endl;
		}
	}
	system("pause");
}
