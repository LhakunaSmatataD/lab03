/**
* This is the file which calculates the number of large sized schools
*
* @author	Abdul Hadi 
*
*/

#include <iostream>
#include<fstream>
#include<vector>
#include<string>

using namespace std;

/**
* @class main
* This class implements the size of school to be large or not
*/

int main()
{
	string string1;

	ifstream file;
	file.open("C:/Users/Ahadi/Desktop/Tehsil.csv");

	///initailises vector
	vector <string> vector;
	
	/**
	* This function obtains the file's contents
	*
	* @return Successful if file opens, Error otherwise
	*/
	while (getline(file, string1, ','))
	{
		vector.push_back(string1);
	}

	float numerator = 0;
	float denominator = 0;
	///creates for loop which checks all schools with more than 50 students in the 9th
	for (int i = 21; i < vector.size(); i += 13) {
		if (stoi(vector[i]) > 50) {
			numerator++;
		}
		denominator++;
	}
	///prints percentage by dividing all large schools with total schools and multiplying by a 100
	cout << ((numerator / denominator) * 100) << endl;
	system("PAUSE");
	return 0;
}
