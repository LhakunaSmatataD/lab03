/**
* @file Course.h
* This file calculates the greatest percentage of dropout students
*
* @author Abdul Hadi
*
*/

#include <iostream>
#include<fstream>
#include<vector>
#include<string>

using namespace std;
int main()
{
	string string1;

	ifstream file;
	file.open("C:/Users/Ahadi/Desktop/Tehsil.csv");

	vector <string> vector;

	while (getline(file, string1, ','))
	{
		vector.push_back(string1);
	}

	float DroppedOutStudents = 0;
	string DroppedName;

	for (int i = 21; i < vector.size(); i += 13) {
		if (stoi(vector[i]) > 50) {
			if (stoi(vector[i + 6]) > DroppedOutStudents) {
				DroppedOutStudents = stoi(vector[i + 6]);
				DroppedName = vector[i - 5];
			}
		}
	}
	cout << "The greatest Dropout Percentage was " << DroppedOutStudents << "% from the School: " << DroppedName << endl;

	system("pause");
}
